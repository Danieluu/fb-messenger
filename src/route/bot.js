const router = require('express').Router();
const { repo } = require('../repo/mongo');
const { MessengerClient } = require('messaging-api-messenger').default;


var config = {
    verify_token: 'howmay',
    token: 'EAAicdDiru4MBALuZCNClwkasTSkceMk46iPXcHJ6WuUUlwApl1oP16ZAlnDAQcmePXXoRlGWe4sKUS6XBxBQLtzEwuZBsdZBj3qerDyy6T7WCs21V1Lc7ZBlBsJ8E7MTvzHsYG8hdttHHaht1DAMbs6AbiIN0CZBioYutpwI1IYXFLX2j04B2q'
};


router.get('/:id', (req, res) => {
    if (
        req.query['hub.mode'] === 'subscribe' &&
        req.query['hub.verify_token'] === config.verify_token
    ) {
        res.send(req.query['hub.challenge']);
    } else {
        console.log('Falied validation. Make sure the validation tokens match.');
        res.sendStatus(403);
    }
});


router.post('/:id', (req, res) => {
    let messaging = req.body.entry[0].messaging[0];
    console.log(messaging);
    let _id = req.params['id'];
    repo.findOne('User', { _id }).then(val => {
        let client = MessengerClient.connect(val.token);
        // client.sendText(messaging.sender.id, 'Get That.');
        client.sendButtonTemplate(
            messaging.sender.id, "What do you want to do next?",[
                {
                    type: 'postback',
                    title: 'hit me',
                    payload: 'somebody hit me!'
                }
            ]
        );
    }).then(() => res.end()).catch(err=> console.log(err));
});

module.exports = router;


// ! If any errors are found, Webhook will stop for about 10 minutes, then start again, and the message will be received after the transfer stops.