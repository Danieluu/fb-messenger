const { MongoClient } = require('mongodb');
const { mongodb } = require('../config/config');


async function insertOne(collection, data) {
    const client = await MongoClient.connect(mongodb.host, { useNewUrlParser: true, useUnifiedTopology: true });
    await client.db(mongodb.db).collection(collection).insertOne(data)
        .then(() => console.log('1 document insert into collection'))
        .catch(() => console.log('insert err'));
    client.close();
}

async function findAll(collection) {
    const client = await MongoClient.connect(mongodb.host, { useNewUrlParser: true, useUnifiedTopology: true });
    const result = await client.db(mongodb.db).collection(collection).find({}).toArray()
        .catch(() => console.log('find all error'));
    client.close();
    return result;
}

async function findOne(collection, query) {
    const client = await MongoClient.connect(mongodb.host, { useNewUrlParser: true, useUnifiedTopology: true });
    const result = await client.db(mongodb.db).collection(collection).findOne(query)
        .catch(() => console.log('find on error'));
    client.close();
    return result;
}

module.exports.repo = { insertOne, findAll, findOne };