const server = require('express')();
const bodyparser = require('body-parser');
const botRouter = require('./route/bot');


server.use(bodyparser.json());
server.use('/', botRouter)

module.exports = server;